//a vulnerable c program to explain common vulnerability types
//fuzz with AFL

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#include "imgRead.h"

struct __attribute__((__packed__)) LSD {
    u_int16_t width;
    u_int16_t height;
    uint8_t LSD_packed; // Logical Screen Descriptor
    uint8_t BCI; //Background Color Index
    uint8_t PAR; //Pixel Aspect Ratio
};

struct __attribute__((__packed__)) GifImage
{
	char header[6];
	struct LSD lsd;
};

uint8_t buff[512] = {0};

# define FIX_BUF_LEN 16
# define FIX_BUF_LEN2 512


void stdout_buf(int x, char *buf) {
    printf("[AFTER] buffer_one is at %p and contains \'%s\'\n", buf, buf);
    printf("[AFTER] value is at %p and is %d (0x%08x)\n", &x, x, x);
}

void vuln_func2(uint8_t* data, uint32_t len) {
    // int value = 5;
    char buffer_two[256];

    // strcpy(buffer_two, "two"); //Put "two" in buffer two

    // printf("[BEFORE] buffer_two is at %p and contains \'%s\'\n", buffer_two, buffer_two);
    // printf("[BEFORE] value is at %p and is %d (0x%08x)\n", &value, value, value);

    // printf("\n[STRCPY] copying %u bytes into buffer_two\n\n", len);
    strcpy(buffer_two, (const char*)data); //copy first argument into buffer two.

    // printf("[AFTER] buffer_two is at %p and contains \'%s\'\n", buffer_two, buffer_two);
    // printf("[AFTER] value is at %p and is %d (0x%08x)\n", &value, value, value);

}

// int ProcessImage(char* filename){

// 	FILE *fp;
// 	char ch;
// 	struct GifImage img;

// 	fp = fopen(filename,"r");            //Statement   1

// 	if(fp == NULL)
// 	{
// 		printf("\nCan't open file or file doesn't exist.");
// 		exit(0);
// 	}

// 	int bytes_read = fread(&img, 1, sizeof(img), fp); // read 13 bytes

// 	int no_of_elems = 0;
// 	if ((img.lsd.LSD_packed & 0b10000000) == 0b10000000 ) {
//         no_of_elems = (img.lsd.LSD_packed & 0b00000111) + 1;

//         u_int32_t gct_size = 3 * pow(2, no_of_elems);
//         u_int8_t *gct = malloc(gct_size);
//         bytes_read = fread(gct, 1, gct_size, fp); // read 12 bytes

//         printf("Global Color Table found. Size: 3 * pow(2, %u) = %d\n", no_of_elems, bytes_read);

//         free(gct);
//     }

// 	u_int8_t sent[16];

// 	bytes_read = fread(&sent, 1, 16, fp); // read 16 bytes

// 	if (sent[0] == ',' || sent[0] == '!' || sent[0] == ';') {
// 	    printf("Valid GIF File.\n");
// 	    if (sent[1] == 0xff) {

//             bytes_read = fread(&buff, 1, 512, fp);
//             vuln_func2(buff, 256);
// 	    }
// 	}

// 	fclose(fp);

// 	return 0;
// }

int ProcessImage(char * filename) {
	FILE *fp;
	fp = fopen(filename,"r");            //Statement   1
	if(fp == NULL)
	{
		printf("\nCan't open file or file doesn't exist.");
		exit(0);
	}

    int fileSize = ftell(fp);
    uint8_t* fileBuffer = malloc(fileSize);

	size_t bytes_read = fread(fileBuffer, 1, fileSize, fp); //read full file

    return ProcessImageBuffer(fileBuffer, bytes_read);

}


int ProcessImageBuffer(uint8_t* data, size_t size){
	char ch;
	struct GifImage img;
    int offset = 0;

    if (offset + sizeof(img) > size) {
        return 0;
    }

	memcpy(&img, data + offset, sizeof(img)); // read 13 bytes
    offset += sizeof(img);

	int no_of_elems = 0;
	if ((img.lsd.LSD_packed & 0b10000000) == 0b10000000 ) {
        no_of_elems = (img.lsd.LSD_packed & 0b00000111) + 1;

        u_int32_t gct_size = 3 * pow(2, no_of_elems);
        uint8_t *gct = malloc(gct_size);

        if (offset + gct_size > size) {
            return 0;
        }

        memcpy(gct, data+offset, gct_size); // read 12 bytes
        offset += gct_size;

        printf("Global Color Table found. Size: 3 * pow(2, %u) = %d\n", no_of_elems, gct_size);

        free(gct);
    }

	uint8_t sent[16];

    if (offset + FIX_BUF_LEN > size ) {
        return 0;
    }

	memcpy(&sent, data + offset, FIX_BUF_LEN); // read 16 bytes

	if (sent[0] == ',' || sent[0] == '!' || sent[0] == ';') {
	    printf("Valid GIF File.\n");
	    if (sent[1] == 0xff) {
            if (offset + FIX_BUF_LEN2 > size) {
                memcpy(&buff, data + offset, size-offset);
            } else {
                memcpy(&buff, data + offset, FIX_BUF_LEN2);
            }
            vuln_func2(buff, 256);
	    }
	}
    return 0;
}
// int main(int argc,char **argv)
// {
//     return ProcessImage(argv[1], 2);
// // 		last parameter is to change between different vulnerability i.e.stack and heap overflow
// //		ProcessImage("/private/tmp/val-corpus/input/0a64c2cd8ec0a34bcb893820c2912ab374660e65.gif" /*argv[1]*/);
// }
