// Example of a standalone runner for "fuzz targets".
// It reads all files passed as parameters and feeds their contents
// one by one into the fuzz target (LLVMFuzzerTestOneInput).
// This runner does not do any fuzzing, but allows us to run the fuzz target
// on the test corpus (e.g. "imgRead_test_data") or on a single file,
// e.g. the one that comes from a bug report.

#include <stdlib.h>
#include <stdio.h>
#include "imgRead.h"

// Forward declare the "fuzz target" interface.
// We deliberately keep this inteface simple and header-free.
extern int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size);

int main(int argc, char **argv)
{

    for (int i = 1; i < argc; i++)
    {
        FILE *fp;
        fp = fopen(argv[i], "r"); //Statement   1
        if (fp == NULL)
        {
            printf("\nCan't open file or file doesn't exist.");
            exit(0);
        }

        int fileSize = ftell(fp);
        uint8_t *fileBuffer = malloc(fileSize);

        size_t bytes_read = fread(fileBuffer, 1, fileSize, fp); //read full file

        return LLVMFuzzerTestOneInput(fileBuffer, bytes_read);
    }

    return 0;
}
