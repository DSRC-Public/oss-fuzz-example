#include<stdio.h>
#include<assert.h>
#include "imgRead.h"

void TestProcessImage(char* filename, int expected) {
    int result = ProcessImage(filename);
    assert(result == expected);
}

int main(int argc, char **argv) {
    
    TestProcessImage("/Users/yogeshkulkarni/Work/Code/oss-fuzz-example/imgRead_test_data/0a0b300905392a5a7b0fbc1fbf90c8e7b378bd0b.gif", 0);

    TestProcessImage("/Users/yogeshkulkarni/Work/Code/oss-fuzz-example/imgRead_test_data/0a00ced4480f1430414876521077aefcfb0fc37b.gif", 0);

    TestProcessImage("/Users/yogeshkulkarni/Work/Code/oss-fuzz-example/imgRead_test_data/0a02f2dea010ee1fa7f073e41302313ce03d8706.gif", 0);

    return 0;
}