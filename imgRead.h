#include <stdio.h>
#include <stdint.h>

int ProcessImage(char* filename);

int ProcessImageBuffer(uint8_t* data, size_t size);