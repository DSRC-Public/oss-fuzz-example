
# Simple example of a build file that nicely integrates a fuzz target
# with the rest of the project.
#
# We use 'make' as the build system, but these ideas are applicable
# to any other build system

# By default, use our own standalone_fuzz_target_runner.
# This runner does no fuzzing, but simply executes the inputs
# provided via parameters.
# Run e.g. "make all LIB_FUZZING_ENGINE=/path/to/libFuzzer.a"
# to link the fuzzer(s) against a real fuzzing engine.
#

# OSS-Fuzz will define its own value for LIB_FUZZING_ENGINE.
LIB_FUZZING_ENGINE ?= standalone_fuzz_target_runner.o
# CC = clang
# CFLAGS += -c

all: imgRead_unittest imgRead_fuzzer

#Unit tests
imgRead_unittest: imgRead_unittest.c imgRead.a
	$(CC) $(CFLAGS) $< imgRead.a -o $@

# Fuzz target, links against $LIB_FUZZING_ENGINE, so that
# you may choose which fuzzing engine to use.
imgRead_fuzzer: imgRead_fuzzer.c imgRead.a standalone_fuzz_target_runner.o
	${CC} ${CFLAGS} $< imgRead.a ${LIB_FUZZING_ENGINE} -o $@
	zip -q -r imgRead_fuzzer_seed_corpus.zip imgRead_test_data

clean:
	rm -fv *.a *.o *unittest *_fuzzer *_seed_corpus.zip crash-* *.zip

imgRead.a: imgRead.c imgRead.h
	${CC} ${CFLAGS} $< -c
	ar ruv imgRead.a imgRead.o

# The standalone fuzz target runner.
standalone_fuzz_target_runner.o: standalone_fuzz_target_runner.c
